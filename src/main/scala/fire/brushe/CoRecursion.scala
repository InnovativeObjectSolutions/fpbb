package fire.brushe

import scala.collection.immutable.Stream.cons


class SmallInt(i:Int) {
  private def apply(x:Int) = new SmallInt(x %10)

  def multiply(b:Int):SmallInt = apply(i*b)

  override def toString():String = "{"+i+"}"
}
object CoRecursion {

  type smallz = SmallInt
  def fibs(cur:Int,prev:Int):Stream[Int] = cons(cur,fibs(cur+prev,cur))


  def main(args: Array[String]): Unit = {
    val result = fibs(1,0).iterator.takeWhile(p => p < 1000)

    println("***");
    println(result.toList)
    val small = new SmallInt(50009)
    println(foo(small,45))

  }

  def foo(a:smallz,b:Int):smallz = a multiply b

}
