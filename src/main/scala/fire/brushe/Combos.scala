package fire.brushe

import scala.concurrent.Future

trait Combos[F[_]] {
  def combine[A](a:F[A],b:F[A])(f:(A,A) => A):F[A]
  def find[A](a:F[A])(f:A => Boolean):F[A]
  def in_sequence[A,B,C](f:A=>B)(g:B=> C):(A => C) = {a => g(f(a))}

}
case class Award(year:Int,awardType:String, amount:Double, recipient:String)
class ListCombos extends Combos[List] {
  override def combine[A](a: List[A], b: List[A])(f: (A, A) => A): List[A] = a ++ b

  override def find[A](a: List[A])(f: (A) => Boolean): List[A] = a filter f
}

class MyLibrary(listUtil:ListCombos){

  def findExpiredAwards(awards:List[Award]):List[Award] = listUtil.find(awards)(award => award.year < 2017 )
  def findAwardsForStudent(name:String,awards:List[Award]):List[Award] =listUtil.find(awards)(award => award.recipient.equals(name))
}