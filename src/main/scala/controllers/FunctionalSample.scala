package controllers

import scala.util._

/**
  *
  *
  *
    *public ResponseEntity<?> createAcademicProfile(@RequestBody AcademicProfileView academicProfileView) {
 **
 ValidationResult result = academicProfileValidator.validate(academicProfileView);
        *if(result.getIsError()){
            *return new ResponseEntity<>(result,HttpStatus.BAD_REQUEST);
        *}
 **
 try{
            *AcademicProgramProfile savedProfile = academicProfileMgr.saveAcademicProfile(academicProfileView);
            *return new ResponseEntity(savedProfile,HttpStatus.OK);
        *}catch (BusinessRuleViolationException bve){
            *return new ResponseEntity(new ValidationResult(bve.getMessage()),
                    *HttpStatus.INTERNAL_SERVER_ERROR);
        *}
        *catch (Exception e){
            *logger.error(e.getMessage());
            *return new ResponseEntity(new ValidationResult("Oops!"),
                    *HttpStatus.INTERNAL_SERVER_ERROR);
        *}
 **
 *
 *
 }
  */
case class Profile(name:String,id:Option[Int])
trait ValidationResult[T]

case class ValidationSuccess[T](t:T) extends ValidationResult[T]
case class ValidationError[T](errors:Map[String,String]) extends ValidationResult[T]



object FunctionalSample {

  def validate(prof:Profile):ValidationResult[Profile] = {
    if(prof.name.length<5){
      ValidationSuccess(prof)
    }
    else{
      ValidationError(Map("name" -> "too short"))
    }
  }

  def save(profile: Profile):Try[Profile] = {
     if(profile.name.eq("FP_intro")){
       Success(profile)
     }
     else
       Failure(new Exception("bad_name"))
  }

  def end(prof:Profile):(Int,Object) = {
    validate(prof) match {
      case ValidationSuccess(goodProf) => save(goodProf).map(_ => (200,"ok")).getOrElse((500,"internal_server"))
      case ValidationError(errors) => (400,errors)
    }
  }
}
