package ice.skirig

trait NRank[F[_],G[_]] {
  def convert[A](a:F[A]):G[A]
}

trait Foldable[F[_]] {
  def fold[A](as:F[A],z:A,f:((A,A) => A)):A
}
class StreamToList extends NRank[Stream,List] {
  override def convert[A](a: Stream[A]): List[A] = a.toList
}

object ListFold extends Foldable[List] {
  override def fold[A](as: List[A], z: A,f:((A,A) => A)): A = as.foldRight(z)(f)
}
object PolyUtils {
  type Id[A] = A

  val streamConverter = new StreamToList()
  val intList = streamConverter.convert(Stream(1,5))
  val stringList = streamConverter.convert(Stream("hello"))

  def intSum:((Int,Int) => Int) = {(a,b) => a + b}
  def stringConcat:((String,String) => String) = {(a,b) => a + b}
  ListFold.fold(List[Int](1,4,5,6),0,intSum)
  ListFold.fold(List[String]("cod","web"),"",stringConcat)


  //doesnt compile
  //def apply[A,B](f: A => List[A], b: B, s: String): (List[B], List[String]) = (f(b), f(s))
}
