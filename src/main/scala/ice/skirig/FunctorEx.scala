package ice.skirig

class Kv[A](list:List[A]) {
  def map[B](f: A => B):Kv[B] = {
    val bList:List[B] = for{
      x <- list
    }yield f(x)
    new Kv(bList)
  }
}
object FunctorEx {


  val listFunctor = new Functor[List] {
      override def map[A, B](fa: List[A])(f: A => B): List[B] =  fa map f
  }
  val kvFunctor = new Functor[Kv]{
    override def map[A, B](fa: Kv[A])(f: A => B): Kv[B] = fa map f
  }


}
