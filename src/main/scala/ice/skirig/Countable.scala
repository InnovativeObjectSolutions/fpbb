package ice.skirig

trait Countable[F[_]] {
  def size[A](as:F[A]):Int
}
object ListCountable extends Countable[List] {
  override def size[A](as: List[A]): Int = as.size
}

object SetCountable extends Countable[Set]{
  override def size[A](as: Set[A]): Int = as.size
}

