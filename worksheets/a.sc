import scala.util.Random

//impure function
val rand = new Random
def isHeads(): Boolean ={

  println("Printing the next random boolean value") //side effect
  rand.nextBoolean() //global state
}
isHeads()

//pure function
def isHeadsPF(rand:Random):(Boolean,Random) = (rand.nextBoolean(),rand)

isHeadsPF(rand)._1

//function as a variable
val charCount:(String => Int) = _.length
val hash:(String => Int) = _.hashCode
charCount("hello")
val strngs = List("hello","cod","world")

//function as a parameter
strngs.map(charCount)

// function as a type
type fnType = (String => Int)

//function as a return type
def toyFactory(bool:Boolean):(String => Int) = if(bool){charCount} else { hash }

toyFactory(true)("hello")
toyFactory(false)("hello")

def intAdd(x:Int,y:Int):Int = x + y
intAdd(4,5)

//function composition
def f1:(String,String) => Int = {(s1,s2) => intAdd(charCount(s1),charCount(s2))}

f1("f","p")






