def imperativeDouble(arr:Array[Int]):Array[Int] = {

  var result = Array.fill(arr.length){0}
  for(idx <- 0 to arr.length-1){
    result(idx) = 2 * arr(idx);
  }
  return result;
}
val arr = Array(3,5,0)
val doubles1 = imperativeDouble(arr)
doubles1.foreach(println)

def functionalDouble(arr:Array[Int]):Array[Int] = {
  arr.map(el => 2 * el)
}
val doubles2 = functionalDouble(arr)
doubles2.foreach(println)

def imperativeProduct(arr:Array[Int]):Int = {
  var result = 1
  for(idx <- 0 to arr.length-1){
    result = result * arr(idx);
  }
  return result;
}

def functionalProduct(arr:Array[Int]):Int = {
  arr.foldRight(1)(_ * _)
}

println(imperativeProduct(arr.take(2)))
println(functionalProduct(arr.take(2)))