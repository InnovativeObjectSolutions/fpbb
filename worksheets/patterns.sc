//Strategy pattern, pass function as a parameter
//Dependency injection

type fn = Int => Int

def add1:fn = {x => x + 1}
def times2:fn = { x => x *2}

val plus1 = (add1)(1)
val mult2 = (times2)(1)


//Decorator Pattern

def isEven:(Int => Boolean) ={ x =>  x %2 ==0 }
//Prints integers
def intLogger:((Int => Boolean),Int) => Boolean = {(f,in) => {
   println("Input is " + in)
  f(in)
}}

isEven(5)
def isEvenWithLogging:(Int => Boolean) = {x =>  intLogger(isEven,x)}
isEvenWithLogging(5)