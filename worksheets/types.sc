type fn = Int => Int

def add1:fn = {x => x + 1}
def times2:fn = { x => x *2}

trait ProgramParadigm
case object OOP extends ProgramParadigm
case object FP extends ProgramParadigm

trait SkillLevel
case object Novice extends SkillLevel
case object Intermediate extends SkillLevel
case object Expert extends SkillLevel
//sum type
type Skillz  = SkillLevel
//sum type
type Tech = ProgramParadigm

//algebraic data type
// product ADT
case class CODDev(tech:Tech,level:Skillz)

type us = CODDev

CODDev(FP,Expert)
CODDev(FP,Intermediate)
CODDev(FP,Novice)

CODDev(OOP,Expert)
CODDev(OOP,Intermediate)
CODDev(OOP,Novice)

