sealed abstract class Constraint(val verifier: Int => Boolean)

def isPrime(n:Int):Boolean = {
  def checkDivisible(divisor:Int):Boolean = {
    if(divisor==1){
      return true
    }
    n%divisor!=0 && checkDivisible(divisor-1)
  }
  checkDivisible(n/2)
}
def isEven(n:Int):Boolean = n%2==0
case object SmallEvens extends Constraint(x => x <=10 && isEven(x) )

object SmallPrimes{
  val smallPrimes = Seq(2,3,5,7)

}


val aSmallPrime = SmallPrimes

def findSmallEvens(ints:List[Int]):List[Int] = ints.filter(n => SmallEvens.verifier(n))

findSmallEvens(List(1,10,4,6,3,7))