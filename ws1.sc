
class Foo {
  class Bar
}
val foo1:Foo = new Foo
val foo2:Foo = new Foo


val foo1Bar1:foo1.Bar = new foo1.Bar
//type foo1.Bar does not conform to expected type foo2.Bar
//val foo2Bar:foo2.Bar = new foo1.Bar
trait Baz {
  type T
  def value(): T

}

object BazInt extends Baz{
  override type T = Int

  override def value(): Int = 4
}

object BazString extends Baz {
  override type T = String

  override def value() = "Meh"
}
BazInt.value()
BazString.value()



