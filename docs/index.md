# Index


## 0
 
* First class functions
    * Pure functions
        * Only depends on inputs
        * No side effect
    * Functions can be
        * variables
        * parameters
        * interfaces
        * return types
* Types
    * bool
    * int
    * string
    * ADT
        * t1 X t2
        * t1 | t2
    
* Composition
    * compose functions
    * can be used to implement all popular patterns
    
* Immutable Data
 
## 1

* Common functional patterns
    * immutable
    * total functions
    * function composition
    * pattern matching
    * optics
 
* Functor 
  * Functor laws

* Monoid 
  * Monoid laws
  
* Monad
  * Monad laws
  
* Recursion
    *  Tail recursion
    * Corecursion
        * Corecursive functions can always evaluate more of the result in a finite amount of time

## 2

 * Category Theory 101
 * Higher kinded types 
 * Type class
 * Algebraic Data Types
 * Dependent types
    * f: value -> type
 * type level programming
 
# References
 * https://fsharpforfunandprofit.com/fppatterns/
 * http://lambdaconf.us/downloads/documents/lambdaconf_slfp.pdf
 * https://tylermcginnis.com/imperative-vs-declarative-programming/