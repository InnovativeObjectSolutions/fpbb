Why not continue to use OO programming? 
 * Inherently depends on mutable state 
 * Thread syncrhonization is ugly
 
Why functional programming matters?
 * immutable data --> inherently thread safe
 * no side effects --> concurrency
   * order of execution doesn't matter
 * composable pieces --> enables DRYness & readable code